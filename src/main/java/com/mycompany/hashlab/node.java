/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hashlab;

import static java.lang.Math.sqrt;

/**
 *
 * @author informatics
 */
public class Node {

    private int key;
    private String value;
    private String[] arr = new String[100];
    private final double c = (sqrt(5) - 1) / 2; //constant value for hash

    // multiplication method hash
    private int hash(int key) {
        return (int) Math.floor(arr.length * (key * c % 1));
    }

    public void put(int key, String value) {
        arr[hash(key)] = value;
    }

    public String get(int key) {
        return arr[hash(key)];
    }

    public void remove(int key) {
        arr[hash(key)] = null;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hashlab;

import static java.lang.Math.sqrt;

/**
 *
 * @author informatics
 */
public class mainHash {

    public static void main(String[] args) {

        Node node = new Node();//Create empty node
        System.out.println("Show get value before put (key = 11)");
        System.out.println(node.get(11));
        System.out.println("Show get value before put (key = 233)");
        System.out.println(node.get(233));
        System.out.println("");

        //put value in node
        node.put(11, "Mango");
        node.put(233, "Yellow");

        //get value by using key
        System.out.println("Show get value after put (key = 11)");
        System.out.println(node.get(11));
        System.out.println("Show get value after put (key = 233)");
        System.out.println(node.get(233));
        System.out.println("");

        //remove value
        node.remove(233);
        System.out.println("Show get value after remove (key = 233)");
        System.out.println(node.get(233));

    }

}
